// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155Receiver.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "./ERC721Full.sol";
import "./IProxyNFT.sol";

contract MetalinqNFT is
    IProxyNFT,
    ReentrancyGuard,
    ERC721Full,
    IERC1155Receiver,
    AccessControl
{
    using SafeMath for uint256;
    bytes32 public constant BUNDLER_ROLE = keccak256("BUNDLER_ROLE");

    uint256 currentProxyTokenID;
    uint256 currentStakedId;
    string public description;
    uint256 public stakedNFTsCount;
    string private _uri;
    address private _owner;
    address public metalinqFeeAddress = address(0);
    uint256 public protocolFeePercent = 0;

    /// @dev Struct to store Staked Data
    struct Staked {
        uint256 price;
        string ipfsHash;
        uint256 copies;
        uint256 mintCount;
    }

    /// @dev maps stakedId with details
    mapping(uint256 => Staked) public stakedData;
    /// @dev maps stakedId with delegates
    mapping(uint256 => Delegate[]) public stakedDelegates;
    /// @dev maps tokenId with  stakedId
    mapping(uint256 => uint256) private _proxyToStakedId;
    /// @dev maps proxyTokenId with its staked price by Id
    mapping(uint256 => Delegate[]) public proxyDelegates;
    /// @dev maps proxyTokenId with corresponding delegate balance
    mapping(uint256 => mapping(address => mapping(uint256 => uint256)))
        public tokenBalances;
    mapping(address => mapping(uint256 => uint256)) public stakedBalances;

    modifier onlyOwner() {
        _checkOwner();
        _;
    }
    event Mint(uint256 proxyTokenID, uint256 stakedId, address minter);

    constructor(
        string memory _name,
        string memory _symbol,
        string memory _description,
        address bundlerAddress,
        string memory _baseUri
    ) ERC721Full(_name, _symbol) {
        description = _description;
        _uri = _baseUri;
        _owner = msg.sender;
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(BUNDLER_ROLE, msg.sender);
        _setRoleAdmin(BUNDLER_ROLE, DEFAULT_ADMIN_ROLE);
        _setupRole(BUNDLER_ROLE, bundlerAddress);
    }

    function getAmount(uint256 bps, uint256 amount)
        public
        pure
        returns (uint256)
    {
        return SafeMath.div(SafeMath.mul(amount, bps), getMaxBPS());
    }

    function getMaxBPS() public pure returns (uint256) {
        return 10000;
    }

    function updateStakedPricesByBatch(
        uint256[] memory stakedIds,
        uint256[] memory prices
    ) external onlyOwner {
        require(stakedIds.length == prices.length, "array length mismatach");
        for (
            uint256 index = 0;
            index < stakedIds.length;
            index = index.add(1)
        ) {
            stakedData[stakedIds[index]].price = prices[index];
        }
    }

    function getProxyTokenData(uint256 tokenId)
        external
        view
        returns (Delegate[] memory, Staked memory)
    {
        return (proxyDelegates[tokenId], stakedData[_proxyToStakedId[tokenId]]);
    }

    function delegateTokens(uint256 proxyTokenId)
        external
        view
        returns (Delegate[] memory)
    {
        return proxyDelegates[proxyTokenId];
    }

    function setStakedNFTsData(
        string[] memory ipfsHashes,
        uint256[] memory prices,
        Delegate[][] memory stakedDelegateData,
        uint256[] memory copies
    ) external onlyOwner {
        require(
            (ipfsHashes.length == prices.length) &&
                (prices.length == stakedDelegateData.length) &&
                (stakedDelegateData.length == copies.length),
            "array lengths mismatch"
        );
        for (
            uint256 index = 0;
            index < ipfsHashes.length;
            index = index.add(1)
        ) {
            uint256 stakedId = currentStakedId.add(1);
            stakedData[stakedId].ipfsHash = ipfsHashes[index];
            stakedData[stakedId].price = prices[index];
            stakedData[stakedId].copies = copies[index];
            stakedData[stakedId].mintCount = 0;
            _validateAndStoreStakedDelegateData(
                stakedId,
                stakedDelegateData[index]
            );
            stakedNFTsCount = stakedNFTsCount.add(1);
            currentStakedId = stakedId;
        }
    }

    function getNextTokenId() public view returns (uint256) {
        return currentProxyTokenID.add(1);
    }

    /// @notice Query if a contract implements an interface
    /// @param interfaceId The interface identifier, as specified in ERC-165
    /// @dev Interface identification is specified in ERC-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceId` and
    ///  `interfaceId` is not 0xffffffff, `false` otherwise
    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(AccessControl, ERC721Full, IERC165)
        returns (bool)
    {
        return
            AccessControl.supportsInterface(interfaceId) ||
            ERC721Full.supportsInterface(interfaceId);
    }

    function mint(uint256 stakedId) external payable nonReentrant {
        Staked memory tokenData = stakedData[stakedId];
        require(tokenData.copies != 0, "stakedId doesn't exist");
        require((tokenData.mintCount < tokenData.copies), "max tokens minted");
        uint256 stakedPrice = tokenData.price;
        require(msg.value == stakedPrice, "invalid amount");

        uint256 _tokenId = getNextTokenId();

        _proxyToStakedId[_tokenId] = stakedId;
        stakedData[stakedId].mintCount = stakedData[stakedId].mintCount.add(1);
        this.initBundle(stakedDelegates[stakedId]);
        _incrementProxyTokenID();
        _safeMint(msg.sender, _tokenId);
        _setTokenURI(_tokenId, Strings.toString(_tokenId));
        _distributeFees();

        emit Mint(_tokenId, stakedId, msg.sender);
    }

    function initBundle(Delegate[] memory delegateData) external {
        uint256 proxyTokenId = getNextTokenId();
        for (
            uint256 index = 0;
            index < delegateData.length;
            index = index.add(1)
        ) {
            proxyDelegates[proxyTokenId].push(delegateData[index]);
        }
    }

    function bundle(Delegate[] memory delegateData, uint256 proxyTokenID)
        external
        nonReentrant
    {
        require(
            hasRole(BUNDLER_ROLE, msg.sender) ||
                ownerOf(proxyTokenID) == msg.sender,
            "caller neither bundler nor owner"
        );
        for (uint256 i = 0; i < delegateData.length; i = i.add(1)) {
            _ensureBundlingConditions(delegateData[i], proxyTokenID);
            address contractAddress = delegateData[i].contractAddress;
            uint256 tokenId = delegateData[i].tokenId;
            uint256 quantity = delegateData[i].quantity;

            tokenBalances[proxyTokenID][contractAddress][
                tokenId
            ] = tokenBalances[proxyTokenID][contractAddress][tokenId].add(
                quantity
            );

            if (_isERC721(contractAddress)) {
                ERC721Full erc721Instance = ERC721Full(contractAddress);
                erc721Instance.transferFrom(msg.sender, address(this), tokenId);
            } else if (_isERC1155(contractAddress)) {
                ERC1155Supply erc1155Instance = ERC1155Supply(contractAddress);
                erc1155Instance.safeTransferFrom(
                    msg.sender,
                    address(this),
                    tokenId,
                    quantity,
                    ""
                );
            }
        }
        emit Bundled(proxyTokenID, delegateData, ownerOf(proxyTokenID));
    }

    function unBundle(Delegate[] memory delegateData, uint256 proxyTokenID)
        external
        nonReentrant
    {
        require(
            (ownerOf(proxyTokenID) == msg.sender),
            "caller not proxy owner"
        );
        for (uint256 i = 0; i < delegateData.length; i = i.add(1)) {
            require(
                _ensureUnBundlingConditions(delegateData[i], proxyTokenID),
                "delegate not assigned to proxy"
            );
            address contractAddress = delegateData[i].contractAddress;
            uint256 tokenId = delegateData[i].tokenId;
            uint256 quantity = delegateData[i].quantity;

            _updateDelegateBalances(delegateData[i], proxyTokenID);

            if (_isERC721(contractAddress)) {
                ERC721Full erc721Instance = ERC721Full(contractAddress);
                erc721Instance.transferFrom(address(this), msg.sender, tokenId);
            } else if (_isERC1155(contractAddress)) {
                ERC1155Supply erc1155Instance = ERC1155Supply(contractAddress);
                erc1155Instance.safeTransferFrom(
                    address(this),
                    msg.sender,
                    tokenId,
                    quantity,
                    ""
                );
            }
        }
        emit Unbundled(proxyTokenID, delegateData);
    }

    function _ensureBundlingConditions(
        Delegate memory delegateData,
        uint256 proxyTokenID
    ) internal view {
        Delegate[] memory storedData = proxyDelegates[proxyTokenID];
        for (uint256 i = 0; i < storedData.length; i = i.add(1)) {
            if (
                delegateData.contractAddress == storedData[i].contractAddress &&
                delegateData.tokenId == storedData[i].tokenId
            ) {
                uint256 balance = tokenBalances[proxyTokenID][
                    delegateData.contractAddress
                ][delegateData.tokenId];
                require(
                    balance.add(delegateData.quantity) <=
                        storedData[i].quantity,
                    "assigned delegate qty exceeds"
                );
                return;
            }
        }
        revert("delegate not assigned to proxy");
    }

    function _ensureUnBundlingConditions(
        Delegate memory delegateData,
        uint256 proxyTokenID
    ) internal view returns (bool) {
        Delegate[] memory storedData = proxyDelegates[proxyTokenID];
        for (uint256 i = 0; i < storedData.length; i = i.add(1)) {
            if (
                delegateData.contractAddress == storedData[i].contractAddress &&
                delegateData.tokenId == storedData[i].tokenId
            ) {
                uint256 balance = tokenBalances[proxyTokenID][
                    delegateData.contractAddress
                ][delegateData.tokenId];
                require(
                    delegateData.quantity <= balance,
                    "qty exceeds balance"
                );
                return true;
            }
        }
        return false;
    }

    function _updateDelegateBalances(
        Delegate memory delegateData,
        uint256 proxyTokenID
    ) internal {
        address contractAddress = delegateData.contractAddress;
        uint256 tokenId = delegateData.tokenId;
        tokenBalances[proxyTokenID][contractAddress][tokenId] = tokenBalances[
            proxyTokenID
        ][contractAddress][tokenId].sub(delegateData.quantity);
    }

    function _distributeFees() internal {
        uint256 commissionCost = SafeMath.div(
            SafeMath.mul(msg.value, protocolFeePercent),
            100
        );
        _sendAmount(commissionCost, metalinqFeeAddress);
    }

    function _sendAmount(uint256 amount, address receiver) internal {
        if (amount > 0) {
            (bool success, ) = payable(receiver).call{value: amount}("");
            require(success, "Send amount Failure");
        }
    }

    function _validateAndStoreStakedDelegateData(
        uint256 stakedId,
        Delegate[] memory data
    ) internal {
        for (uint256 i = 0; i < data.length; i = i.add(1)) {
            // require(
            //     data[i].contractAddress != address(0),
            //     "invalid contractaddress"
            // );
            bool isERC721 = _isERC721(data[i].contractAddress);
            if (isERC721) {
                require(data[i].quantity == 1, "ERC721 qty must be 1");
            }
            require(
                isERC721 || _isERC1155(data[i].contractAddress),
                "invalid contractaddress"
            );
            stakedDelegates[stakedId].push(data[i]);
        }
    }

    function _baseURI() internal view override returns (string memory) {
        return _uri;
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if the sender is not the owner.
     */
    function _checkOwner() internal view virtual {
        require(owner() == msg.sender, "Ownable: caller is not the owner");
    }

    function _isERC1155(address contractAddress) internal view returns (bool) {
        return IERC1155(contractAddress).supportsInterface(0xd9b67a26);
    }

    function _isERC721(address contractAddress) internal view returns (bool) {
        return IERC721(contractAddress).supportsInterface(0x80ac58cd);
    }

    function _incrementProxyTokenID() internal {
        currentProxyTokenID = currentProxyTokenID.add(1);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal virtual override(ERC721Full) {
        super._beforeTokenTransfer(from, to, tokenId);
        uint256 stakedId = _proxyToStakedId[tokenId];
        if (from != address(0))
            stakedBalances[from][stakedId] = stakedBalances[from][stakedId].sub(
                1
            );
        stakedBalances[to][stakedId] = stakedBalances[to][stakedId].add(1);
    }

    function onERC1155Received(
        address operator,
        address from,
        uint256 id,
        uint256 value,
        bytes calldata data
    ) external pure override returns (bytes4) {
        return this.onERC1155Received.selector;
    }

    function onERC1155BatchReceived(
        address operator,
        address from,
        uint256[] calldata ids,
        uint256[] calldata values,
        bytes calldata data
    ) external pure override returns (bytes4) {
        return this.onERC1155BatchReceived.selector;
    }
}
